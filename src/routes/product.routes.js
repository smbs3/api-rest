import { Router } from "express";
import { productValidation, updateProductValidation } from "../validation";
import {
  getProducts,
  createProducts,
  getProductId,
  editProducts,
  deleteProducts,
} from "../controllers/products.controller";

const router = Router();

router.get("/", getProducts);
router.get("/:id", getProductId);
router.post("/", productValidation, createProducts);
router.put("/:id", updateProductValidation, editProducts);
router.delete("/:id", deleteProducts);

export default router;
