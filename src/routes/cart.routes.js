import { Router } from "express";
import { cartDetailValidation } from "../validation";
import {
  addProduct,
  createCart,
  deleteCart,
  getCart,
  getCarts,
  removeProduct,
} from "../controllers/cart.controller";

const router = Router();

router.get("/", getCarts);
router.post("/", createCart);
router.get("/:id", getCart);
router.post("/remove-product", cartDetailValidation, removeProduct);
router.post("/add-products", cartDetailValidation, addProduct);
router.delete("/:id", deleteCart);

export default router;
