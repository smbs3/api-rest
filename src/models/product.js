import { DataTypes } from "sequelize";
import {sequelize} from "../databases/database";
// import { Cart } from "./cart";

export const Products = sequelize.define("products",
 {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING,
  },
  price: {
    type: DataTypes.FLOAT,
  },
  image: {
    type: DataTypes.STRING,
  },
  category: {
    type: DataTypes.ENUM,
    values: ["BURGER", "NOODLES", "ICE_CREAM", "CHICKEN", "DEFAULT"],
  },
  discount: {
    type: DataTypes.INTEGER,
  },
},
{ paranoid: true }
);

// Products.belongsToMany(Cart, { through: 'cart_detail' });