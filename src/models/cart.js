import { DataTypes } from "sequelize";
import {sequelize} from "../databases/database";
// import Products from "./product";

export const Cart = sequelize.define("carts", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
  },
},
{ paranoid: true });
// Cart.belongsToMany(Products, { through: 'cart_detail' });