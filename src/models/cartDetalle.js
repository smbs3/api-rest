import { DataTypes } from "sequelize";
import { sequelize } from "../databases/database";
import { Cart } from "./cart";
import { Products } from "./product";


export const CartDetalle = sequelize.define("cart_detail", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
  },
},
  {
    paranoid: true,
  }
);

Cart.belongsToMany(Products, {
  through: CartDetalle,
});
Products.belongsToMany(Cart, {
  through: CartDetalle,
});


