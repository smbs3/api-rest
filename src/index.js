import express from "express";
import morgan from "morgan";
import dontenv from "dotenv";
import cors from "cors";
import cartRoutes from "./routes/cart.routes";
import productRoutes from "./routes/product.routes"

dontenv.config();

const app = express();

app.set('PORT', process.env.API_PORT || 3000);
app.use(morgan('combined'));
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());
app.use('/cart',cartRoutes);
app.use('/products',productRoutes);

app.listen(app.get('PORT'), () => {
    console.log(`Server listening ${app.get('PORT')}`)
  })