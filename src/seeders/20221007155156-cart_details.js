"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert("cart_details", [
      {
        id: "347ab6ff-aeb2-4f3f-8b4e-23ff49b95620",
        cartId: "5e9f5882-f1b0-4357-a56b-28031669153e",
        productId: "ab9a2ffa-c777-487f-9c8f-dd7ff0f8e221",
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("carts", null, {});
  },
};
