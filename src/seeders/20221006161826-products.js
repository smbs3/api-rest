"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert("products", [
      {
        id: "ab9a2ffa-c777-487f-9c8f-dd7ff0f8e221",
        name: "full chicken grill",
        image: "https://foodingly.netlify.app/assets/img/tab-img/item7.png",
        price: 60,
        category: "CHICKEN",
        discount: 35,
      },
      {
        id: "20ecb13c-755e-45b0-91f4-0a3d5db0d2bf",
        name: "Double Cheese Burger",
        image: "https://foodingly.netlify.app/assets/img/tab-img/item1.png",
        price: 59,
        category: "BURGER",
        discount: 50,
      },
      {
        id: "abf8095a-34f5-49df-8458-1d2f494569fc",
        name: "Chicken Cutlet",
        image: "https://foodingly.netlify.app/assets/img/tab-img/item5.png",
        price: 32,
        category: "CHICKEN",
      }
    ]);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("products", null, {});
  },
};
