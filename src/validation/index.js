import Joi from "joi";

export const productValidation = (req, res, next) => {
  const productSchema = Joi.object().keys({
    name: Joi.string().required(),
    price: Joi.number().required(),
    image: Joi.string().required(),
    category: Joi.string()
      .valid("CHICKEN", "ICE_CREAM", "DRINKS", "BURGER", "NOODLES")
      .uppercase(),
    discount: Joi.number().integer(),
  });

  validateRequest(req, res, next, productSchema);
};

export const cartDetailValidation = (req, res, next) => {
  const cartDetailSchema = Joi.object().keys({
    cartId: Joi.string().required(),
    productId: Joi.string().required(),
  });

  validateRequest(req, res, next, cartDetailSchema);
};

export const updateProductValidation = (req, res, next) => {
  const productSchema = Joi.object().keys({
    name: Joi.string(),
    price: Joi.number(),
    image: Joi.string(),
    category: Joi.string()
      .valid("CHICKEN", "ICE_CREAM", "DRINKS", "BURGER", "NOODLES")
      .uppercase(),
    discount: Joi.number().integer(),
  });

  validateRequest(req, res, next, productSchema);
};
export const validateRequest = (req, res, next, schema) => {
  const options = {
    abortEarly: false,
    allowUnknown: true,
    stripUnknown: true,
  };
  const { error, value } = schema.validate(req.body, options);
  if (error) {
    return res.status(400).send(error.details.map((x) => x.message).join(","));
  }
  req.body = value;
  next();
};
