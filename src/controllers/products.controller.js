
import { Products } from "../models/product";

export const getProducts = async (req, res) => {
  try {
    const products = await Products.findAll();
    res.json(products);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const createProducts = async (req, res) => {
  const { name, price, image, discount, category } = req.body;
  try {
    const newProduct = await Products.create({
      name,
      image,
      price,
      discount,
      category,
    });
    res.json(newProduct);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getProductId = async (req, res) => {
  try {
    const { id } = req.params;
    const product = await Products.findOne({
      where: {
        id,
      },
    });
    res.json(product);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const editProducts = async (req, res) => {
  try {
    const { id } = req.params;
    const { name, image, price, discount, category } = req.body;
    const product = await Products.findByPk(id);
    product.name = name;
    product.image = image;
    product.price = price;
    product.category = category;
    product.discount = discount;

    await product.save();

    res.json(product);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
export const deleteProducts = async (req, res) => {
  try {
    const { id } = req.params;
    await Products.destroy({
      where: {
        id,
      },
    });
    res.sendStatus(204);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
