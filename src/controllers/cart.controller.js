import { Cart } from "../models/cart";
import { CartProducts } from "../models/cartDetalle";
import { Product } from "../models/product";

export const getCarts = async (req, res) => {
  try {
    const cart = await Cart.findAll();
    res.json(cart);
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
};

export const getCart = async (req, res) => {
  try {
    const { id } = req.params;
    const cart = await Cart.findOne({
      where: {
        id,
      },
      include: {
        model: Product,
        through: {
          attributes: [],
        },
      },
    });

    if (!cart) return res.status(404).json({ message: "Cart does not exist" });

    res.json(cart);
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
};

export const createCart = async (req, res) => {
  try {
    const cart = await Cart.create({});
    res.json(cart);
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
};

export const deleteCart = async (req, res) => {
  try {
    const { id } = req.params;
    await Cart.destroy({
      where: {
        id,
      },
    });
    res.sendStatus(204);
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
};

export const addProduct = async (req, res) => {
  try {
    const { cartId, productId } = req.body;
    await CartProducts.create({
      cartId,
      productId,
    });
    res.sendStatus(204);
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
};

export const removeProduct = async (req, res) => {
  try {
    const { cartId, productId } = req.body;
    await CartProducts.destroy({
      where: {
        cartId,
        productId,
      },
    });
    res.sendStatus(204);
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
};
