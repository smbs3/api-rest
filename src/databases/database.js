import Sequelize  from "sequelize";
import dbConfig from "../../config/config";

const config = dbConfig[process.env.NODE_ENV];

export const sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  config
);


